package br.com.itau;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Maquina {

    private int quantidadeSlots;
    private List<Simbolos> simbolos;
    private int pontuacaoFinal;
    private double fatorMultiplicadorPontos;

    public Maquina(int quantidadeSlots, double fatorMultiplicadorPontos) {
        this.quantidadeSlots = quantidadeSlots;
        simbolos = new ArrayList<Simbolos>();
        this.fatorMultiplicadorPontos = fatorMultiplicadorPontos;
    }

    public double getFatorMultiplicadorPontos() {
        return fatorMultiplicadorPontos;
    }

    public void setFatorMultiplicadorPontos(double fatorMultiplicadorPontos) {
        this.fatorMultiplicadorPontos = fatorMultiplicadorPontos;
    }

    public int getQuantidadeSlots() {
        return quantidadeSlots;
    }

    public void setQuantidadeSlots(int quantidadeSlots) {
        this.quantidadeSlots = quantidadeSlots;
    }

    public List<Simbolos> getSimbolos() {
        return simbolos;
    }

    public void setSimbolos(List<Simbolos> simbolos) {
        this.simbolos = simbolos;
    }

    public int getPontuacaoFinal() {
        return pontuacaoFinal;
    }

    public void setPontuacaoFinal(int pontuacaoFinal) {
        this.pontuacaoFinal = pontuacaoFinal;
    }

    public void sortearSimbolos() {
        Random r = new Random();
        for (int i = 0; i < this.quantidadeSlots; i++) {
            Simbolos simboloAux = Simbolos.values()[r.nextInt(Simbolos.values().length - 1)];
            simbolos.add(simboloAux);
        }
    }

    public void calcularPontuacao() {
        boolean multiplica = verificaSimbolosIguais();

        for (Simbolos s : simbolos
        ) {
            this.pontuacaoFinal += s.valor;
        }

        if(multiplica)
                this.pontuacaoFinal *= this.fatorMultiplicadorPontos;
    }

    private boolean verificaSimbolosIguais() {
        if (simbolos != null)
            return simbolos.stream().filter(s -> s.valor == simbolos.get(0).valor).count() == simbolos.size();
        else return false;
    }

    @Override
    public String toString() {
        String retorno = "";
        if (simbolos != null) {
            for (int i = 0; i < simbolos.size(); i++) {
                retorno += simbolos.get(i).name() + "(" + simbolos.get(i).valor + ") |";
            }

            retorno += "Pontuacao final: " + this.pontuacaoFinal;
        }

        return retorno;
    }
}

