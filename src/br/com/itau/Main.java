package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Maquina maquina = new Maquina(3, 100);
        maquina.sortearSimbolos();

        /* TESTE COM SIMBOLOS IGUAIS
        List<Simbolos> l = new ArrayList<Simbolos>();
        l.add(Simbolos.BANANA);
        l.add(Simbolos.BANANA);
        l.add(Simbolos.BANANA);
        maquina.setSimbolos(l);
        */
        
        maquina.calcularPontuacao();

        System.out.println("Items sorteados: " + maquina.toString());
    }
}
