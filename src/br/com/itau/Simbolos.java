package br.com.itau;

public enum Simbolos {
    BANANA(10), FRAMBOESA(50), MOEDA(100), SETE(300);

    public int valor;

    Simbolos(int valor) {
        this.valor = valor;
    }
}
